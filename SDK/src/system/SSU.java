package system;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jbpm.test.JBPMHelper;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeEnvironmentBuilder;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.manager.RuntimeManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import controllers.ControladorServicios;
import controllers.ControladorjBPM;
import dao.DAOProcesos;
import dao.DAOServicioContextual;
import model.Indicador;
import model.Paciente;
import model.Proceso;
import model.ServicioContextual;

/**
 * Controlador que se encarga de gestionar todo el proceso de solicitud 
 * y ejecución de servicios de servicios de salud ubicuos. 
 * @author Javier
 */
public class SSU {
	RuntimeEngine engine;
	@Autowired
	DAOServicioContextual dao_servicios;
	@Autowired
	ControladorServicios controlador_servicios;
	@Autowired
	DAOProcesos dao_procesos;
	@Autowired
	ControladorjBPM jbpm;
	
	public SSU() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * Inicializa contenedor de procesos BPMN y crea una instancia nueva del RuntimeEngine
	 */
	@PostConstruct
	public void init(){
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieBase kbase = kContainer.getKieBase("kbase");
		RuntimeManager manager = createRuntimeManager(kbase);
		engine = manager.getRuntimeEngine(null);
	}

	/**
	 * Método estático que configura los Enviroments y Handlers necesarios para
	 * la configuración del Runtime Manager.
	 * @param kbase Base de conocimiento con procesos
	 * @return RuntimeManager instanciado
	 */
	private static RuntimeManager createRuntimeManager(KieBase kbase) {
		JBPMHelper.startH2Server();
		JBPMHelper.setupDataSource();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("org.jbpm.persistence.jpa");
		RuntimeEnvironmentBuilder builder = RuntimeEnvironmentBuilder.Factory.get()
		.newDefaultBuilder().entityManagerFactory(emf).knowledgeBase(kbase);
		return RuntimeManagerFactory.Factory.get()
		.newSingletonRuntimeManager(builder.get(), "com.sample:example:1.0");
	}
	
	/**
	 * Solicita servicios de salud ubicuos
	 * @param indicadores Indicadores del paciente
	 * @param _paciente Paciente
	 * @return Map con respuesta de la ejecución de servicios
	 */
	public Map<String,Object>solicitarServicios(Set<Indicador> indicadores, Paciente _paciente){
		Paciente paciente = _paciente;
		Map<String, Object> response =  new LinkedHashMap<String,Object>();
		/*Chequeamos integridad de los objetos recibidos*/
		if(indicadores == null || _paciente == null){
			throw new  NullPointerException("Los datos enviados deben tener valor no nulo");
		}
		
		Set <ServicioContextual> servicios = consultarServicios(paciente);
		/*Si el set está vacío, respondo de inmediato al cliente que no se encontraron
		 * servicios contextuales
		 */
		if(servicios.isEmpty()){
			response = new LinkedHashMap<String,Object>();
			response.put("status",1);
			response.put("process",null);
			response.put("message","No se encontraron servicios de detecci�n de contexto"
					+ "adecuados");
			return response;
		}
		
		Set <String> contextos_paciente = ejecutarServicios(servicios, indicadores);
		paciente.setContextoAltoNivel(contextos_paciente);
		
		ArrayList<Proceso> procesos = consultarProcesos(paciente,indicadores);
		/*Si la lista está vacía, entonces aviso que no se encontraron procesos*/
		if(procesos.isEmpty()){
			response = new LinkedHashMap<String,Object>();
			response.put("status",1);
			response.put("process",null);
			response.put("message","No se encontraron procesos u-health"
					+ "para ejecutar");
			return response;
		}	
		/*Ejecutamos primer proceso en la lista. Si no se puede ejecutar por alguna razón,
		 * registramos y ejecutamos el siguiente en la lista.
		 */
		for(ListIterator<Proceso> it = procesos.listIterator(); it.hasNext();){
			Proceso proceso = it.next();
			Map <String, Object> input = construirInput(proceso,paciente,indicadores);
			response = jbpm.ejecutarProceso(proceso,input, engine);
			if((Integer)response.get("status") == 1){
				break;
			}
		}
		return response;	
	}
	/**
	 * Solicita servicios de salud ubicuos solo con la información del paciente.
	 * @param paciente Paciente.
	 * @return Map con respuesta de la ejecución de los servicios.
	 */
	public Map<String, Object> solicitarServicios(Paciente paciente){
		Map<String,Object> response = new LinkedHashMap<String,Object>();
		ArrayList<Proceso> procesos = consultarProcesos(paciente,null);
		/*Si la lista está vacía, entonces aviso que no se encontraron procesos*/
		if(procesos.isEmpty()){
			response = new LinkedHashMap<String,Object>();
			response.put("status",1);
			response.put("process",null);
			response.put("message","No se encontraron procesos u-health"
					+ "para ejecutar");
			return response;
		}	
		/*Ejecutamos primer proceso en la lista. Si no se puede ejecutar por alguna razón,
		 * registramos y ejecutamos el siguiente en la lista.
		 */
		for(ListIterator<Proceso> it = procesos.listIterator(); it.hasNext();){
			Proceso proceso = it.next();
			Map <String, Object> input = construirInput(proceso,paciente,null);
			response = jbpm.ejecutarProceso(proceso,input, engine);
			if((Integer)response.get("status") == 1){
				break;
			}
		}
		return response;	
	}
	
	/**
	 * Obtiene conjunto de servicios a ejecutar según el contexto del paciente
	 * @param paciente Paciente
	 * @return Set con servicios
	 */
	private Set<ServicioContextual> consultarServicios(Paciente paciente){
		dao_servicios.setServicios(paciente);
		return dao_servicios.getServicios();
	}
	
	/**
	 * Ejecuta servicios de detección de contexto entregados
	 * @param services Servicios a ejecutar
	 * @param indicadores Indicadores asociados al paciente
	 * @return Set con contextos detectados
	 */
	private Set<String> ejecutarServicios(Set<ServicioContextual> services, Set<Indicador> indicadores){
		return controlador_servicios.ejecutarServicios(services, indicadores);
	}
	/**
	 * Genera lista de procesos u-health coincidentes con contexto del paciente.
	 * @param paciente Paciente.
	 * @param indicadores Conjunto de indicadores del paciente.
	 * @return ArrayList con procesos obtenidos.
	 */
	private ArrayList<Proceso> consultarProcesos(Paciente paciente, Set<Indicador> indicadores){
		dao_procesos.setProcesos(paciente);
		ArrayList<Proceso> procesos = dao_procesos.getProcesos();
		if(!procesos.isEmpty()){
			procesos = validarInput(procesos,paciente,indicadores);
		}
		return procesos;
	}
	
	/**
	 * Construye input a enviar a Proceso U-Health. 
	 * @param proceso Proceso de negocio a ejecutar
	 * @param paciente Paciente
	 * @param indicadores Indicadores del paciente
	 * @return Input solicitado por el proceso.
	 */
	private Map<String, Object> construirInput(Proceso proceso, Paciente paciente, Set<Indicador> indicadores){
		Proceso process = proceso;
		Map <String, String> input_data = process.getInputdata();
		Map <String, Object> input = new LinkedHashMap<String,Object>();
		for (Iterator<String> it = input_data.keySet().iterator();it.hasNext();){
			String input_name = it.next();
			switch(input_name){
			case "Paciente":
				input.put("Paciente",paciente);
			case "Indicadores":
				if(indicadores!=null){
					input.put("Indicadores",indicadores);
				}
				else {
					input.clear();
					return input;
				}
					
			}	
		}
		return input;	
	}

	/**
	 * Comprueba que para c/u de los procesos seleccionados, existe la información
	 * solicitada como input
	 * @param _procesos Conjunto de procesos u-health
	 * @param paciente Paciente para el que se desea realizar validación
	 * @param indicadores Información de indicadores del paciente.
	 * @return ArrayList con procesos que pasan validación
	 */
	private ArrayList<Proceso> validarInput (ArrayList<Proceso> _procesos, Paciente paciente, Set<Indicador> indicadores){
		ArrayList<Proceso> procesos = _procesos;
		for (ListIterator<Proceso> it = procesos.listIterator();it.hasNext();){
			Proceso process = it.next();
			/*Si el input data no es null entonces el proceso tiene un input*/
			if(process.getInputdata() != null){
				/*Si el Map retornado por construirInput() está vacío, entonces no se dispone
				de toda la información para ejecutar el proceso. Lo removemos.*/
				if(construirInput(process, paciente, indicadores).isEmpty()){
					it.remove();
				}
			}
		}
		return procesos;
	}
}
	
