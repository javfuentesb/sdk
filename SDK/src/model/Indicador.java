package model;
import org.json.JSONObject;


/**
 * Clase abstracta que define la informacion y comportamiento de un Indicador
 * @author Javier
 *
 */
public abstract class Indicador {
	String URI; /*URI del indicador en la ontologia*/
	String Nombre;/*Nombre del indicador. Corresponde al de la clase*/
	String Tipo; /*Tipo de indicador: Bioseñal u otra*/
	String Data_URI; /*URI o path de la data*/
	String Datatype; /*Tipo  de dato en el que se almacena la data*/
	
	public Indicador(String URI, String tipo, String data_URI, String datatype) {
		super();
		this.URI = URI;
		Nombre = this.getClass().getSimpleName();
		Tipo = tipo;
		Data_URI = data_URI;
		Datatype = datatype;
	}
	
	public String getName(){
		return Nombre;
	}
	/**
	 * Obtiene información de indicadores en representaci�n JSON.
	 * @return JSONObject con información de indicadores.
	 */
	public abstract JSONObject getDataasJSON();
	
	/**
	 * Obtiene información de indicadores en formato XML.
	 * @return String en formato XML con información de indicadores.
	 */
	public abstract String getDataasXML();
	
	/**
	 * Obtiene información de indicadores en formato CSV.
	 * @return String en formato CSV con información de indicadores.
	 */
	public abstract String getDataasCSV();

	
}
