package model;

import java.util.Set;

/**
 * Clase Paciente. Modela la informacion básica define a un Paciente.
 * @author Javier
 *
 */
public class Paciente {
	private String uri; /*URI del recurso paciente en la ontología*/
	private String nombre; /*Nombre del paciente*/
	private String telefono; /*Número de teléfono móvil del paciente*/
	private String domicilio; /*Domicilio del paciente*/
	private String email; /*Correo electrónico del paciente*/
	private Set<String> contexto_medico; /*Set de contextos médicos del paciente*/
	private Set<String> contexto_altonivel; /*Set de contextos de alto nivel*/
	private Set<String> indicadores; /*Set de indicadores asociados del paciente*/
	private Set<String> cuidadores; /*Conjunto de cuidadores asociados al paciente*/
	private Set<String> medicos; /*Conjunto de médicos asociados al paciente*/
	
	public Paciente(String uri, String nombre, String telefono, String domicilio, String email,
			Set<String> contexto_medico, Set<String> indicadores,
			Set<String> cuidadores, Set<String> medicos) {
		super();
		this.uri = uri;
		this.nombre = nombre;
		this.telefono = telefono;
		this.domicilio = domicilio;
		this.email = email;
		this.contexto_medico = contexto_medico;
		this.indicadores = indicadores;
		this.cuidadores = cuidadores;
		this.medicos = medicos;
	}
	
	public Set<String> getIndicadores() {
		return indicadores;
	}
	public String getID() {
		return uri;
	}
	public String getNombre() {
		return nombre;
	}
	public Set<String> getContextoMedico() {
		return contexto_medico;
	}
	public Set<String> getContextoAltoNivel() {
		return contexto_altonivel;
	}
	public Set<String> getContexto_medico() {
		return contexto_medico;
	}
	public Set<String> getCuidadores() {
		return cuidadores;
	}
	public Set<String> getMedicos() {
		return medicos;
	}
	public void setContextoAltoNivel(Set<String> contextos_paciente) {
		this.contexto_altonivel = contextos_paciente;
		
	}
}
