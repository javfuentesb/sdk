package model;
import java.util.Map;


public class Proceso {
	private String Name; /*Nombre del proceso*/
	private String id; /* ID del proceso*/
	private Map<String,String> input_data; /*Map con nombre de variable y tipo de dato del input*/
	private Map<String,String> output_data; /*Map con nombre de variable y tipo de dato del output*/
	private String type; /*Tipo de proceso*/
	private double similarity; /*Atributo de similaridad con el contexto del paciente*/
	
	public Proceso(String name, String id, Map<String,String> input, Map <String,String> output, String type, double _similarity) {
		super();
		Name = name;
		this.id = id;
		input_data = input;
		output_data = output;
		this.type = type;
		similarity= _similarity;
	}
	
	public Proceso(String name, String id, Map<String,String> input, Map <String,String> output, String type) {
		super();
		Name = name;
		this.id = id;
		input_data = input;
		output_data = output;
		this.type = type;
	}
	
	public String getID() {
		return Name;
	}
	
	public String getPath() {
		return id;
	}
	
	public double getSimilarity() {
		return similarity;
	}
	
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
	public Map<String, String> getInputdata() {
		return input_data;
	}
	
	public Map<String, String> getOutputdata() {
		return output_data;
	}

	public String getType() {
		return type;
	}
}
