package model;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Encapsula la informaci�n que define a un Servicio de Detecci�n de Contexto y su
 * ejecuci�n.
 * @author Javier
 *
 */
public class ServicioContextual {
	private String id; /*id del servicio en la ontología*/
	private String name; /*Nombre del servicio*/
	private String URI; /*URI del servicio en la web*/
	private Set <String> Indicadores; /*Conjunto de indicadores a los que está asociado el servicio*/
	private String InputType; /*Tipo de input*/
	private String RequestType; /*Tipo de petición HTTP: PUT, POST, etc.*/
	
	public ServicioContextual(String id, String uri, Set<String> indicadores, String inputType,
			String requestType) {
		super();
		this.id = id;
		this.name = id.replace('_', ' ');
		this.URI = uri;
		Indicadores = indicadores;
		InputType = inputType;
		RequestType = requestType;
	}
	
	public String getID() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getInputType() {
		return InputType;
	}
	public void setInputType(String inputType) {
		InputType = inputType;
	}
	public String getURI() {
		return URI;
	}
	public void setURI(String uRI) {
		URI = uRI;
	}
	public String getRequestType() {
		return RequestType;
	}
	public void setRequestType(String requestType) {
		RequestType = requestType;
	}
	public Set<String> getIndicadores() {
		return Indicadores;
	}
	public void setIndicadores(Set<String> indicadores) {
		Indicadores = indicadores;
	}
	
	/**
	 *Solicita la ejecución de servicios de detección de contexto REST
	 * @param indicadores Set de Indicadores monitoreados al paciente.
	 * @return Set de Strings que representan los contextos de alto nivel activos en el 
	 * paciente.
	 * @throws JSONException Manejo de excepción al utilizar JSONObjects
	 */
	public Set <String> ejecutar(Set <Indicador> indicadores) throws JSONException{
		
		Set <String> ind_servicio = Indicadores;
		Set <Indicador> ind_paciente = indicadores;
		Set <String> contextos = new LinkedHashSet<>();
	
		//Eliminamos indicadores que no forman parte de los solicitados por el servicio
		for(Iterator<Indicador> iter_ind_paciente = ind_paciente.iterator();
				iter_ind_paciente.hasNext();){
			Indicador act_indicador = iter_ind_paciente.next();
			boolean coincidence = false;
			for(Iterator<String> iter_ind_servicio = ind_servicio.iterator();
					iter_ind_servicio.hasNext();){
				String indicador_servicio= iter_ind_servicio.next();
				if (indicador_servicio.equals(act_indicador.getName())){
					coincidence = true;
					break;
				}
			}
			if(coincidence == false){
				iter_ind_paciente.remove();
			}
		}
		//Creamos json con información de indicadores
		JSONObject json_request = new JSONObject();
		for(Iterator<Indicador> iter_ind_paciente = ind_paciente.iterator();
				iter_ind_paciente.hasNext();){
			Indicador indicador = iter_ind_paciente.next();
			String name = indicador.getName();
			JSONObject json_ind = indicador.getDataasJSON();
			JSONArray json_ind_array = (JSONArray) json_ind.get(name);
			json_request.append(name,json_ind_array);
		}
		
		//Enviar json a servicio RESTful
		Client client = Client.create(); 
		WebResource webResource = client.resource(URI); /*Seteamos recurso web a hacer petición*/
		ClientResponse response = webResource.type("application/json").
				post(ClientResponse.class, json_request.toString()); /*Enviamos petición*/
		
		/*Obtenemos respuesta*/
		if(response.getStatus() != 201){
			return contextos;
		}	
		String output = response.getEntity(String.class);
		JSONObject json_response = new JSONObject(output);
		
		//Leemos respuesta y depositamos en el Set.
		for(Iterator<?> response_iterator = json_response.keys();response_iterator.hasNext();){
			String contexto = (String) response_iterator.next();
				if(!json_response.get(contexto).equals(null)){
					contextos.add(json_response.getString(contexto).toString());
				}
		}
		return contextos;	
	}

}
