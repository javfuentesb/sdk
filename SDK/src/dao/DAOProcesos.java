package dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Comparator;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

import model.Paciente;
import model.Proceso;

/**
 * DAOProcesos.java
 * Representa el objeto de acceso a datos de los procesos.
 * Permite consultar a la ontología por procesos u-health 
 * y obtener dichos procesos como objetos de clase Proceso.
 * @author Javier
 */
public class DAOProcesos implements IDAOProcesos {
	private String URI_Ontologia; /*URI que identifica a la ontología a consultar como recurso*/
	private String PATH_Ontologia; /* Path de la ontología a utilizar*/
	private ArrayList <Proceso> procesos = new ArrayList <Proceso>(); /*ArrayList con procesos a entregar*/
	private OntModel model;
	private String query_head = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
			"PREFIX owl: <http://www.w3.org/2002/07/owl#>"+
			"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
			"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"+
			"PREFIX : ";

	public DAOProcesos(String _uri_ontologia, String _path_ontologia) {
		/* Mejora: Instanciar ambos fields con anotaciones*/
		super();
		URI_Ontologia = _uri_ontologia;
		PATH_Ontologia = _path_ontologia;
		init();
	}
	
	/**
	 * Carga ontología en memoria
	 * @author Javier
	 */	
	private void init(){
		query_head+=URI_Ontologia;
		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
		String fileName = PATH_Ontologia;
		try {
			File file = new File(fileName);
			FileInputStream reader= new FileInputStream(file);
			model.read(reader,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Consulta y entrega procesos que hacen match con el contexto del paciente
	 * @param paciente El paciente para quien se realiza el setting de procesos.
	 * @return Lista con procesos ordenados de mayor a menor nivel de match semántico.
	 * @author Javier
	 *
	 */
	@Override
	public void setProcesos(Paciente paciente){
		Set<String> procesos_candidatos = generarProcesoscandidatos(paciente);
		Map<String,Double> similitudes = calcularSimilitudes(procesos_candidatos,paciente);
		procesos = (ordenarProcesos(instanciarProcesos(similitudes)));
	}
	/**
	 * Retorna listado de procesos previamente seteados
	 * @return ArrayList con procesos ordenados de mayor a menor similitud
	 * @author Javier
	 *
	 */
	@Override
	public ArrayList<Proceso> getProcesos(){
		return procesos;
	}
	/**
	 * Retorna un proceso específico
	 * @param nombre del proceso
	 * @return Éxito: Proceso solicitado. Fallo: null.
	 * @author Javier
	 *
	 */
	@Override
	public Proceso getProceso(String uri_proceso){
		Proceso next_proceso = null;
		for (Iterator<Proceso> it = procesos.iterator();it.hasNext();){
			next_proceso = it.next();
			if(next_proceso.getID().equals(uri_proceso)){
				return next_proceso;
			}	
		}
		return next_proceso;
	}
	/**
	 * Obtiene un Map con el valor de la similitud entre cada proceso y el paciente
	 * @param procesos_candidatos Set de procesos candidatos
	 * @param paciente Paciente
	 * @return Map<String,Double> con key URI del proceso y Double valor de la similitud
	 */
	private Map<String,Double> calcularSimilitudes(Set<String> procesos_candidatos, Paciente paciente){
		Set<String> patient_context = new LinkedHashSet<>();
		patient_context.addAll(paciente.getContextoAltoNivel());
		patient_context.addAll(paciente.getContextoMedico());
		
		//Map con similitudes de los procesos
		Map <String, Double> similitudes_procesos = new LinkedHashMap <String,Double>();
			
		for(Iterator<String> ordered_iterator = procesos_candidatos.iterator()
			;ordered_iterator.hasNext();){
			Set <String> process_context = new LinkedHashSet <String>();/*Set de contextos del proceso*/
			Set <String> process_contexto_an = new LinkedHashSet <String>(); /*Set de contextos alto nivel del proceso*/
			double similitud = 0; /*Similitud proceso/paciente*/
				
			//Consultamos contexto de alto nivel del proceso
			String proceso = ordered_iterator.next().toString();
			String query_contexto_an = "SELECT ?precondicion ?efecto WHERE{<"
			+proceso+">:tienePrecondicion ?precondicion; :tieneEfecto ?efecto.}"; 
			ResultSet rs = ejecutarConsulta(query_contexto_an);
				
			//Añadimos contexto alto nivel	al Set de contextos del proceso
			while(rs.hasNext()){
				QuerySolution qs = rs.nextSolution();
				if(qs.getResource("?precondicion")!=null){
					process_contexto_an.add(qs.getResource("?precondicion").toString());
				}
				if(qs.getResource("?efecto")!=null){
					process_contexto_an.add(qs.getResource("?efecto").toString());
				}
			}
			//Consultamos contexto medico	
			String query_contexto_medico = "SELECT  ?contexto_medico WHERE{<"
			+proceso+"> :soportaContextoMedico ?contexto_medico.}";
			ResultSet rs_cm = ejecutarConsulta(query_contexto_medico);
				
			//Para cada contexto medico consultamos sus componentes especificas.
			while(rs_cm.hasNext()){
				String contexto_medico = rs_cm.nextSolution().getResource("?contexto_medico").toString();
				if(contexto_medico!=null){
					String query_contextos_final = "SELECT ?enfermedad ?intervencion ?sindrome ?signo WHERE{<"
					+contexto_medico+">:compuestoporEnfermedad ?enfermedad; :compuestoporSindrome ?sindrome;"+
					":compuestoporSindrome ?sindrome; :compuestoporIntervencion ?intervencion; :compuestoporSintomasySignos ?signo.}";
					ResultSet rs_cm_final = ejecutarConsulta(query_contextos_final);
						
					while(rs_cm_final.hasNext()){
						Set <String> process_contexto_medico = new LinkedHashSet <String>();
						QuerySolution qs_final = rs_cm_final.next();
						if(qs_final.getResource("?enfermedad")!=null){
							process_contexto_medico.add(qs_final.getResource("?enfermedad").toString());
						}
						if(qs_final.getResource("?intervencion")!=null){
							process_contexto_medico.add(qs_final.getResource("?intervencion").toString());
						}
						if(qs_final.getResource("?sindrome")!=null){
							process_contexto_medico.add(qs_final.getResource("?sindrome").toString());
						}
						if(qs_final.getResource("?signo")!=null){
							process_contexto_medico.add(qs_final.getResource("?signo").toString());
						}
						/*Añadimos contexto medico y de alto nivel a vector de contextos del proceso*/
						process_context.addAll(process_contexto_medico);
						process_context.addAll(process_contexto_an);
							
						/*Calculamos similitud entre contexto del paciente y del proceso
						 * Nótese que se toma el máximo de entre todos los contextos
						 *  posibles del proceso
						 */
						similitud = Math.max(similitud, calcularSimilitud(patient_context,process_context));
						process_context.clear();
							}
						}
				}
				similitudes_procesos.put(proceso,similitud); 
				/*Añadimos similitud del proceso al set de similitudes*/
				}
		return similitudes_procesos;
	}
	
	/**
	 * Calcula la similitud semántica entre el contexto de un proceso y un paciente
	 * @param contexto_paciente Un Set que reúne todos los contextos activos del paciente.
	 * @param contexto_procesos Un Set que reúne todos los contextos asociados al proceso.
	 * @return Similitud paciente/proceso
	 * @author Javier
	 *
	 */
	private double calcularSimilitud(Set <String> contexto_paciente, Set <String> contexto_procesos){
		Set <String> contextos_paciente = contexto_paciente;
		Set <String> contextos_procesos = contexto_procesos;
		Set <String> context_union = new LinkedHashSet<>();
		context_union.addAll(contextos_procesos);
		context_union.addAll(contextos_paciente);

		double first_sum=0;
		double second_sum=0;
		double third_sum=0;
		
		for(Iterator<String> iterator = context_union.iterator();iterator.hasNext();){
			String next_element= iterator.next().toString();
			double omega_patient=0;
			double omega_process=0;
			if(contextos_paciente.contains(next_element)){
				omega_patient=1;
			}
			if (contextos_procesos.contains(next_element)){
				omega_process=1;
			}
			first_sum=first_sum+omega_patient*omega_process;
			second_sum=second_sum+omega_patient*omega_patient;
			third_sum=third_sum+omega_process*omega_process;
		}
		
		return first_sum/(Math.sqrt(second_sum)*Math.sqrt(third_sum));
	}
	/**
	 * Recibe una lista de URIs de procesos y los instancia como objetos de clase Proceso
	 * @param similitudes_procesos Map con "key" igual a la URI del proceso y "value" 
	 * igual al valor de la similitud de dicho proceso con el paciente actual.
	 * @return Set con Procesos instanciados
	 * @author Javier
	 */
	private Set<Proceso> instanciarProcesos(Map <String, Double> similitudes_procesos){
		Set<Proceso> procesos = new LinkedHashSet<>();
	
		for(Iterator<String> process_iterator = similitudes_procesos.keySet().iterator();
				process_iterator.hasNext();){
			//Siguiente proceso en la lista
			String process= process_iterator.next();
			
			//Inicializamos variables del proceso
			String nombre = null;
			String path = null;
			String type = null;
			Map <String,String> inputs = new LinkedHashMap<String,String>();
			Map <String,String> outputs = new LinkedHashMap<String,String>();
			double similitud = similitudes_procesos.get(process);
	
			//Consultamos literales
			String query = query_head+ "SELECT ?name ?path ?type WHERE{ <"
			+ process+"> :tienePath ?path; :tieneType ?type; :tieneName ?name }";
			ResultSet result_query = ejecutarConsulta(query);
			while(result_query.hasNext()){
				QuerySolution qs = result_query.next();
				if(qs.getLiteral("?name")!=null){
					nombre = qs.getLiteral("?name").toString();
				}
				if(qs.getLiteral("?path")!=null){	
					path = qs.getLiteral("?path").toString();
				}
				if(qs.getLiteral("?type")!=null){
					type = qs.getLiteral("?type").toString();
				}
			}
			//Consultamos inputs y datatypes
			String query_2= query_head+"SELECT ?input ?inputtype WHERE{ <"+
			process+"> :tieneInputdeProceso ?input. ?input :tieneInpuType ?inputtype.}";
			ResultSet result_query2=ejecutarConsulta(query_2);
			while(result_query2.hasNext()){
				try{
					String input_name= result_query2.next().getResource("?input").toString();
					String input_type= result_query2.next().getLiteral("?inputtype").toString();
				inputs.put(input_name,input_type);
				}
				catch (NullPointerException e){
					System.err.println("Error: Input no v�lido en proceso "+process);
				}
			}
			
			String query_3= query_head+"SELECT ?output ?outputtype WHERE{ <"+
					process+"> :tieneOutputdeProceso ?output. ?input :tieneOuputType ?outputtype.}";
			ResultSet result_query3=ejecutarConsulta(query_3);
			while(result_query2.hasNext()){
				try{
					String output_name= result_query3.next().getResource("?output").toString();
					String output_type= result_query3.next().getLiteral("?outputtype").toString();
				outputs.put(output_name,output_type);
				}
				catch (NullPointerException e){
					System.err.println("Error: Output no v�lido en proceso "+process);
				}
			}
			//Instanciamos proceso
			procesos.add(new Proceso(nombre,path,inputs,outputs,type,similitud));
		}
		return procesos;
	}	

	/**
	 * Función que encapsula lógica de consulta a ontología
	 * @param  query Consulta SPARQL
	 * @return ResultSet
	 * @author Javier
	 */
	private ResultSet ejecutarConsulta(String query){	
		Query query_exec= QueryFactory.create(query);
		QueryExecution exec = QueryExecutionFactory.create(query_exec, model);
		ResultSet rs= exec.execSelect();
		return rs;
	}
	/**
	 * Ordena un Set de Procesos de mayor a menor nivel de similitud.
	 * @param Procesos Set de Procesos
	 * @return ArrayList de procesos ordenados.
	 * @author Javier
	 *
	 */
	private ArrayList<Proceso> ordenarProcesos(Set<Proceso> Procesos){
		List <Proceso> procesos = new ArrayList<>();
		procesos.addAll(Procesos);
		Collections.sort(procesos, new Comparator<Proceso>() {
	        @Override
	        public int compare(Proceso proceso1, Proceso proceso2)
	        {
	            return  (int) (proceso1.getSimilarity()-proceso2.getSimilarity());
	        }
	    });
		return (ArrayList<Proceso>) procesos;

	}
	
	/**
	 * Obtiene un conjunto de procesos candidatos a ser seleccionados, de acuerdo 
	 * a la información contextual tanto del paciente como del proceso.
	 * @param paciente Paciente para el que se seleccionan procesos candidatos.
	 * @return Un conjunto de URIs de procesos.
	 * @author Javier
	 */
	private Set<String> generarProcesoscandidatos(Paciente paciente){
				Set <String> contexto_an_paciente = paciente.getContextoAltoNivel(); /*Set con contexto 
				de alto nivel del paciente*/
				Set <String> procesos_candidatos = new LinkedHashSet <>(); /*Set para los procesos candidatos*/
				Set <String> patient_context = new LinkedHashSet <>(); /*Set para los contextos 
				(médico+alto nivel) del paciente*/
				
				//Añadimos ambos tipos de contexto al Set de contextos general
				patient_context.addAll(paciente.getContextoAltoNivel());
				patient_context.addAll(paciente.getContextoMedico());
			
			//PASO 1: Consultamos por procesos con precondición coincidente
				for(Iterator<String> iterador_contextos_an = contexto_an_paciente.iterator();
						iterador_contextos_an.hasNext();){
					String query= "SELECT DISTINCT ?proceso WHERE{?proceso :tienePrecondicion <"+
				iterador_contextos_an.next().toString()+">}";
					ResultSet rs = ejecutarConsulta(query);
					while(rs.hasNext()){
							Resource proceso = rs.next().getResource("?proceso");
							if(proceso!=null){
								procesos_candidatos.add(proceso.toString());
							}
						}
				}
			//PASO 2: Eliminar procesos que no cumplen condición de inclusión
				for(Iterator<String> process_iterator = procesos_candidatos.iterator();
						process_iterator.hasNext();){
					String proceso = process_iterator.next().toString();
					Set <String> precondiciones_proceso= new LinkedHashSet <String>(); /*Declaramos set de precondiciones del proceso*/
					//Consultamos precondiciones asociadas al proceso
					String query_2="SELECT ?precondicion WHERE{<"
					+ proceso
					+"> :tienePrecondicion ?precondicion";
					ResultSet rs_2= ejecutarConsulta(query_head+query_2);
					//A�adimos las precondiciones a la lista
					while (rs_2.hasNext()){
						Resource precondicion =rs_2.nextSolution().getResource("?precondicion");
						if(precondicion!=null){
							precondiciones_proceso.add(precondicion.toString());
						}
					}
					//Si no se cumple relación de inclusión, eliminamos el proceso
					if(!contexto_an_paciente.containsAll(precondiciones_proceso)){
						process_iterator.remove();
					}
				}
		return procesos_candidatos;	
	}
	
}
