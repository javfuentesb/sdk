package dao;
import java.util.Set;

import model.Paciente;
/**
 * Interface definida para definir DAO para Pacientes.
 * @author Javier
 *
 */
public interface IDAOPacientes {
	/**
	 * Obtiene todos los pacientes en la base de datos.
	 * @return Set con los pacientes en el sistema.
	 */
	public Set<Paciente> getAllPacientes();	
	/**
	 * Obtiene un paciente específico utilizando su id en la base de datos.
	 * @param id id del paciente en la base de datos.
	 * @return En el caso exitoso: Paciente solicitado. En caso de fallo: null.
	 */
	public Paciente getPaciente(String id);
}

