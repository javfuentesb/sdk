package dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.ModelFactory;
import org.springframework.beans.factory.annotation.Value;

import model.Paciente;
import model.Proceso;
import model.ServicioContextual;

/**
 * DAOServicioContextual.java
 * Representa el objeto de acceso a datos de Servicios Contextuales
 * Permite consultar a la ontología de contexto por servicios contextuales
 * e instanciar dichos servicios como objetos de clase ServicioContextual
 * @author Javier
 *
 */
public class DAOServicioContextual implements IDAOServicios {
	@Value("${ont_uri}")
	private String URI_Ontologia;
	
	@Value("${ont_path}")
	private String PATH_Ontologia;
	
	private Set <ServicioContextual> servicios_contextuales;
	private OntModel model;
	private String query_head;
	
	public DAOServicioContextual() {
		super();
	}

	/**
	 * Carga ontología en memoria
	 * 
	 */
	@PostConstruct
	private void init(){
		query_head= "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
				"PREFIX owl: <http://www.w3.org/2002/07/owl#>"+
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"+
				"PREFIX : "+ URI_Ontologia+"#";
		
		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
		try {
			System.out.println("Imprimimos path"+ PATH_Ontologia);
			File file = new File(PATH_Ontologia);
			FileInputStream reader= new FileInputStream(file);
			model.read(reader,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Consulta, setea y entrega servicios contextuales que coinciden con la información contextual
	 * estática del paciente.
	 * @param paciente Paciente.
	 * @return ArrayList con Servicios Contextuales.
	 * @author Javier
	 *
	 */
	@Override
	public void setServicios(Paciente paciente){
		Set<String> uri_servicios= seleccionarServiciosContextuales(paciente);
		servicios_contextuales = instanciarServiciosContextuales(uri_servicios);
	}
	
	@Override
	public Set<ServicioContextual> getServicios() {
		// TODO Auto-generated method stub
		return servicios_contextuales;
	}

	@Override
	public ServicioContextual getServicio(String id_servicio) {
		ServicioContextual next_servicio = null;
		for (Iterator<ServicioContextual> it = servicios_contextuales.iterator();it.hasNext();){
			next_servicio = it.next();
			if(next_servicio.getID().equals(id_servicio)){
				return next_servicio;
			}	
		}
		return next_servicio;
	}
	
	/**
	 * Selecciona servicios contextuales que coinciden con el contexto del paciente
	 * @param paciente Paciente.
	 * @return Set de URIs de servicios seleccionados
	 */
	private Set<String> seleccionarServiciosContextuales(Paciente paciente){
		Set <String> indicadores = paciente.getIndicadores(); /*Set de indicadores del paciente*/
		Set <String> servicios = new LinkedHashSet <String>(); /*Set de servicios*/
		Set <String> contextos_p= paciente.getContextoMedico(); /*Set con contexto médico del paciente*/
				
		//PASO 1: Preguntar por los servicios que tengan al menos un indicador del paciente
		String query="";
		//Iteración para armar consulta de servicios
		for (Iterator<String> indicadores_iterator = indicadores.iterator();indicadores_iterator.hasNext();){
			String indicador = indicadores_iterator.next().toString();
			query= "SELECT DISTINCT ?servicio WHERE{ :"+
			indicador+" :esInputde ?servicio}";
			if(indicadores_iterator.hasNext()){
				query=query+"UNION{";
			}
		}
		query = query+"}";
		ResultSet rs_servicios= ejecutarConsulta(query_head+query);
		//Guardamos servicios
		while (rs_servicios.hasNext()){
			String servicio= rs_servicios.nextSolution().getResource("?servicio").toString();
			servicios.add(servicio);
		}
		//PASO 2: Comparar cjto. indicadores del paciente con los de cada servicio 
		//Para cada servicio
		for(Iterator<String> service_iter= servicios.iterator();service_iter.hasNext();){
			String servicio = service_iter.next();
			//Declaramos arraylist con indicadores de servicio
			ArrayList <String> indicadores_servicio= new ArrayList <String>();
			//Consultamos indicadores asociados al servicio
			String query_2="SELECT ?indicador WHERE{ ?indicador :esInputde :"+servicio+"}";
			ResultSet rs_2= ejecutarConsulta(query_head+query_2);
			//Añadimos los indicadores al arraylist
			while (rs_2.hasNext()){
				String indicador =rs_2.nextSolution().getResource("?indicador").toString();
				indicadores_servicio.add(indicador);
			}
			//Si no se cumple relación de inclusión, eliminamos el servicio
			if(!indicadores.containsAll(indicadores_servicio)){
				service_iter.remove();
			}
		}
		//PASO 3: Comparaciones extra: Descarta servicios con contexto contradictorio y servicios duplicados
		//Comparamos contexto para cada servicio
		for(Iterator<String> service_iter= servicios.iterator();service_iter.hasNext();){
			Set <String> contextos_servicio= new LinkedHashSet<String>();
			String servicio=service_iter.next().toString();
			String query_3="SELECT ?contexto WHERE{ :"+servicio+" :asociadoaContextoMedico ?contexto";
			ResultSet rs_3= ejecutarConsulta(query_head+query_3);
					
			//Si el servicio tiene contexto asociado
			if(rs_3.getRowNumber()!=0){
				//Recorremos resultados y guardamos contexto del servicio
				while(rs_3.hasNext()){
					if(rs_3.nextSolution().getResource("?contexto").toString()!=null){
						contextos_servicio.add(rs_3.nextSolution().getResource("?contexto").toString());
					}
				}
				//Si el contexto no está dentro del registrado al paciente lo removemos
					if(!contextos_p.containsAll(contextos_servicio)){
						service_iter.remove();
					}
			}
		}	
			//Eliminamos servicios duplicados
		for(Iterator<String> service_iter= servicios.iterator();service_iter.hasNext();){
			//Consultamos si el servicio es especialización de otro
			String query_4="SELECT ?servicio WHERE{ :"+service_iter.next().toString() +":esEspecializacionde ?servicio";
			ResultSet rs_4= ejecutarConsulta(query_head+query_4);
					
			if(rs_4.getRowNumber()!=0){
				String servicio= rs_4.nextSolution().getResource("?servicio").toString();
					//Si es así, y el servicio padre está en el listado, lo eliminamos
					if(servicios.contains(servicio)){
						service_iter.remove();
					}
				}
			}
		return servicios;
	}
	
	/**
	 * Instancia servicios seleccionados como objetos de clase ServicioContextual
	 * @param context_services Set con los id de los servicios seleccionados
	 * @return Set con servicios contextuales definidos
	 */
	private Set<ServicioContextual> instanciarServiciosContextuales(Set<String> context_services){
		for(Iterator<String> service_iterator=context_services.iterator();
				service_iterator.hasNext();){
			//Siguiente servicio en la lista
			String service_resource= service_iterator.next();
			
			//Inicializamos variables del servicio
			String id = service_resource;
			String URI = null;
			String RequestType = null;
			String InputType = null;
			Set <String> Indicadores = new LinkedHashSet<String>();
	
			//Consultamos literales
			String query= query_head+ "SELECT  ?URI ?RequestType ?InputType WHERE{ <"
			+ service_resource+" :tieneURI ?URI; :tieneInputType ?input; :tieneRequestType ?request;}";
			ResultSet result_query= ejecutarConsulta(query);
			while(result_query.hasNext()){
				QuerySolution qs= result_query.next();
				if(qs.getLiteral("?URI")!=null){
					URI= qs.getLiteral("?URI").toString();
				}
				if(qs.getLiteral("?RequestType")!=null){
					RequestType= qs.getLiteral("?RequestType").toString();
				}
				if(qs.getLiteral("?InputType")!=null){
					InputType= qs.getLiteral("?InputType").toString();
				}
			}
			
			//Consultamos indicadores
			String query_2= query_head+"SELECT ?indicador WHERE{ <"+
			service_resource+"> :tieneIndicador ?indicador.}";
			ResultSet result_query2=ejecutarConsulta(query_2);
			while(result_query2.hasNext()){
				QuerySolution qs = result_query2.next();
				if(qs.getResource("?indicador")!=null){
					String indicador= qs.getResource("?indicador").toString();
					Indicadores.add(indicador);
				}
			}
			//Instanciamos servicio
			servicios_contextuales.add(new ServicioContextual(id,URI,Indicadores,InputType,RequestType));
		}
		return servicios_contextuales;
	}
	/**
	 * Función que encapsula lógica de consulta a ontología
	 * @param  query Consulta SPARQL
	 * @return ResultSet
	 * @author Javier
	 */
	private ResultSet ejecutarConsulta(String query){	
		Query query_exec= QueryFactory.create(query);
		QueryExecution exec = QueryExecutionFactory.create(query_exec, model);
		ResultSet rs= exec.execSelect();
		return rs;
	}


}
