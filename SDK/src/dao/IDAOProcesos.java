package dao;

import java.util.ArrayList;

import model.Paciente;
import model.Proceso;

public interface IDAOProcesos {
	
	public void setProcesos(Paciente Paciente);
	public ArrayList<Proceso> getProcesos();
	public Proceso getProceso(String uri_proceso);

}
