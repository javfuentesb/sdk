package dao;

import java.util.Set;

import model.Paciente;
import model.ServicioContextual;

public interface IDAOServicios {
	
	public void setServicios(Paciente Paciente);
	public Set<ServicioContextual> getServicios();
	public ServicioContextual getServicio(String uri_proceso);

}
