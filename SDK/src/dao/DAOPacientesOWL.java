package dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.ModelFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import model.Paciente;

/** 
 * Implementación de DAOPacientes para ontología modelada en OWL/RDF.
 * @author Javier
 *
 */
@Service
public class DAOPacientesOWL implements IDAOPacientes{
	private Set<Paciente> pacientes;
	@Value("${ont_uri}")
	private String URI_ontologia;
	@Value("${ont_path}")
	private String PATH_ontologia;
	private OntModel model;
	private String head;
	
	private void init(){
		head =  "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
				"PREFIX owl: <http://www.w3.org/2002/07/owl#>"+
				"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
				"PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"+
				"PREFIX : "+ URI_ontologia;
		model = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
		String fileName = PATH_ontologia;
		try {
			File file = new File(fileName);
			FileInputStream reader= new FileInputStream(file);
			model.read(reader,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public DAOPacientesOWL(String URI, String PATH) {
		URI_ontologia = URI;
		PATH_ontologia = PATH;
		init();
		
		String body = "SELECT ?paciente WHERE{ ?paciente a :Paciente}";
		String query = head+body;
		ResultSet rs = ejecutarConsulta(query);
		while(rs.hasNext()){
			String nombre = null;
			String email = null;
			String domicilio = null;
			String telefono = null;
			Set<String> indicadores = new LinkedHashSet<String>();
			Set<String> cuidadores = new LinkedHashSet<String>();
			Set<String> medicos = new LinkedHashSet<String>();
			Set<String> contexto_medico = new LinkedHashSet<String>();
			
			QuerySolution qs = rs.next();
			String uri_paciente = qs.getResource("?paciente").toString();
			
			String query_2 = "SELECT ?nombre ?email ?domicilio ?telefono WHERE{ :"+
						uri_paciente+":tieneNombre ?nombre; :tieneEmail ?email; "
								+ ":tieneDomicilio ?domicilio;" + ":tieneTelefono ?telefono.}"+head;
			ResultSet rs_2 = ejecutarConsulta(query_2);
			while(rs_2.hasNext()){
				QuerySolution qs_2 = rs_2.next();
				if(qs_2.getLiteral("?nombre")!=null){
					nombre = qs_2.getLiteral("?nombre").toString();
				}
				if(qs_2.getLiteral("?email")!=null){
					email = qs_2.getLiteral("?email").toString();
				}
				if(qs_2.getLiteral("?domicilio")!=null){
					domicilio = qs_2.getLiteral("?domicilio").toString();
				}
				if(qs_2.getLiteral("?telefono")!=null){
					telefono = qs_2.getLiteral("?telefono").toString();
				}
			}
			
			String query_3 = "SELECT ?indicador ?medico ?cuidador ?enfermedad ?sindrome"+
			"?sintoma ?intervencion WHERE{ :"+uri_paciente+":tienePlan ?plan;"+
			":tieneContextoMedico ?contextomedico; :esCuidadopor ?cuidador;"+
			":esTratadopor ?medico. ?plan :tieneIndicador ?indicador."+
			" ?contextomedico :compuestoporEnfermedad ?enfermedad; :compuestoporSindrome ?sindrome;"
			+":compuestoporIntervencion ?intervencion; :compuestoporSintomasySignos ?sintoma.}";
			ResultSet rs_3 = ejecutarConsulta(query_3);
			while(rs_3.hasNext()){
				QuerySolution qs_3 = rs_3.next();
				if(qs_3.getResource("?indicador")!=null){
					indicadores.add(qs_3.getResource("?indicador").toString());
				}
				if(qs_3.getResource("?medico")!=null){
					medicos.add(qs_3.getResource("?medico").toString());
				}
				if(qs_3.getResource("?cuidador")!=null){
					cuidadores.add(qs_3.getResource("?cuidador").toString());
				}
				if(qs_3.getResource("?enfermedad")!=null){
					contexto_medico.add(qs_3.getResource("?enfermedad").toString());
				}
				if(qs_3.getResource("?sindrome")!=null){
					contexto_medico.add(qs_3.getResource("?sindrome").toString());
				}	
				if(qs_3.getResource("?sintoma")!=null){
					contexto_medico.add(qs_3.getResource("?sintoma").toString());
				}
				if(qs_3.getResource("?intervencion")!=null){
					contexto_medico.add(qs_3.getResource("?intervencion").toString());
				}
			}
			pacientes.add(new Paciente(uri_paciente,nombre,telefono,domicilio,email,contexto_medico,
					indicadores,cuidadores,medicos));
		}
	}

	@Override
	public Set<Paciente> getAllPacientes() {
		return pacientes;
	}

	@Override
	public Paciente getPaciente(String uri) {
		Paciente next_paciente = null;
		for (Iterator<Paciente> it = pacientes.iterator();it.hasNext();){
			next_paciente = it.next();
			if(next_paciente.getID().equals(uri)){
				return next_paciente;
			}	
		}
		return next_paciente;
	}
	
	/**
	 * Función que encapsula lógica de consulta a ontología
	 * @param  query Consulta SPARQL
	 * @return ResultSet
	 * @author Javier
	 */
	private ResultSet ejecutarConsulta(String query){	
		Query query_exec= QueryFactory.create(query);
		QueryExecution exec = QueryExecutionFactory.create(query_exec, model);
		ResultSet rs= exec.execSelect();
		return rs;
	}
	

}
