package controllers;

import java.util.Map;

import model.Paciente;

public interface IControladorVista {

	public Map<String, Object> solicitarServiciosUbicuos(Paciente paciente, String contexto_an);

	
}
