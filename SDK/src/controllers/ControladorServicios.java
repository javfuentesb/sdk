package controllers;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONException;

import model.Indicador;
import model.ServicioContextual;

/**
 * ControladorServicios.java
 * Se encarga de gestionar la consulta a los servicios contextuales
 * @author Javier
 *
 */

public class ControladorServicios {
	
	/**
	 * Ejecuta un conjunto de Servicios de detección de contexto especificados por el cliente
	 * @param services Servicios a ejecutar
	 * @param indicadores Indicadores del paciente
	 * @return Conjunto de contextos de alto nivel 
	 * @author Javier
	 */
	public Set<String> ejecutarServicios(Set<ServicioContextual> services, Set<Indicador> indicadores){
		Set <String> contextos_altonivel = new LinkedHashSet<String>();
		for(Iterator<ServicioContextual> iterator = services.iterator(); iterator.hasNext();){
			ServicioContextual service = iterator.next();
			try {
				contextos_altonivel.addAll(service.ejecutar(indicadores));
				}
			 catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
		}
		return contextos_altonivel;
		
	}
}
