package controllers;

import java.util.LinkedHashMap;
import java.util.Map;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;

import org.kie.api.runtime.process.WorkflowProcessInstance;

import model.Proceso;

/**
 * Controlador a cargo de la ejecución de procesos de negocio u-health
 * @author Javier
 *
 */
public class ControladorjBPM {
	private Map <String, Object> input; 
	private Map <String, String> output_data; 
	private Map <String,Object> output;	
	
	/**
	 * Ejecuta y retorna la respuesta de un proceso u-health
	 * @param process Proceso de negocio a ejecutar
	 * @param _input Input a enviarle
	 * @return Respuesta del proceso
	 */
	public Map <String, Object>ejecutarProceso(Proceso process, Map <String, Object> _input, RuntimeEngine engine){
		KieSession ksession = engine.getKieSession();/*Obtenemos sesión particular para este proceso*/
		input = _input;
		output_data = process.getOutputdata();
		output = new LinkedHashMap<String, Object>();
		
		try{
		//Invocamos proceso
			WorkflowProcessInstance proceso = (WorkflowProcessInstance) ksession.startProcess(process.getPath(),input);
		// Response genérico
			output.put("status", 1);
			output.put("process", proceso.getProcessName());
		//Obtenemos data de salida
			for( java.util.Iterator<String> it =output_data.keySet().iterator(); it.hasNext();){
				String key = it.next();
				Object  value = proceso.getVariable(key);
				if(value!=null){
					output.put(key,value); 
				}
				else{
					/*Arrojamos excepción avisando que no existe el output pedido*/
					throw new Exception("No existe el output"+ key);
				}
			}
		}
		catch (Exception e){
			e.printStackTrace(System.err); /*Imprimimos StackTrace en salida de errores para posterior debugging*/
			output.clear();
			output.put("status",0);/*Le avisamos al cliente del error*/
			output.put("process", process.getID());
			output.put("message", "Error al ejecutar proceso");
		}
		return output;
	}
}
