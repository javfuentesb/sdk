package controllers;

import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import model.Indicador;
import model.Paciente;
import system.SSU;

/**
 * ControladorWBAN
 * Controlador que encapsula la solicitud de servicios ubicuos y el manejo estándar de respuesta
 * asociados a procesos sin output.
 * Se debe crear una clase concreta por cada paciente que forma parte del sistema
 * @author Javier
 *
 */
public abstract class ControladorWBAN{
	private Set<Indicador> indicadores;
	private Paciente paciente;
	private Map <String, Object> response;
	private final static Logger log = Logger.getLogger(ControladorWBAN.class.getName());
	private SSU ssu;
	
	public ControladorWBAN(SSU ssu) {
		super();
		this.ssu = ssu;
	}
	
	/**
	 *  Método público para el cliente final. Encapsula cada uno de los pasos del algoritmo de ejecuci�n
	 *  y manejo de servicios de salud ubicuos.
	 */
	public void ejecutarServiciosUbicuos(){
		indicadores = obtenerIndicadores();
		response = enviarSolicitud(indicadores,paciente);
		manejarRespuesta(response);
	}
	/**
	 * Se encarga de enviar la solicitud de servicios al controlador responsable
	 * @param indicadores Conjunto de indicadores del paciente
	 * @param paciente Paciente
	 * @return Map con respuesta de SSU
	 */
	public Map<String, Object> enviarSolicitud(Set<Indicador> indicadores, Paciente paciente){
		return ssu.solicitarServicios(indicadores, paciente);
	}
	
	/**
	 * Manejo estándar de respuesta para procesos sin output. Error/éxito es guardado en logger
	 * y se notifica al cliente el status final de la ejecución.
	 * @return int indicando el estado final de la petición realizada. 1 si fue correcta, 0 si hubo alg�n error.
	 * @author Javier
	 *
	 */
	public int manejarRespuesta(Map<String, Object> _response) {
		int status = (int) _response.get("status");
		//Checkeamos que respuesta está ok:
		if((int)_response.get("status")==0){
			log.severe("Error en la ejecución del proceso");
		}
		else{
			log.fine("Proceso ejecutado correctamente");
		}
		return status;
	}
	
/**
 * Encapsula la lógica de obtención de la información de indicadores, señales de interés
 * para conocer el estado de un paciente dado y/o de el o los sistemas que le brindan apoyo.
 * @return un Set con los Indicadores asociados al paciente
 * @author Javier
 *
*/
	abstract Set<Indicador> obtenerIndicadores();
}
